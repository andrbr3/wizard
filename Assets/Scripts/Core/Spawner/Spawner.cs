using System.Collections.Generic;
using Factory;
using UnityEngine;
using VContainer;

namespace Core.Spawner
{
    public abstract class Spawner<T> : MonoBehaviour where T : MonoBehaviour
    {
        [Inject] private IFactory<T> _factory;
    
        [SerializeField] private List<Transform> _spawnPoints;
        [SerializeField] private List<T> _prefabs;
        [SerializeField] private Transform _parent;
    
        private int _spawnIterator;
        private int _prefabIterator;
    
        public T Spawn()
        {
            var obj = _factory.Create(_prefabs[_prefabIterator], _parent);
            obj.transform.position = _spawnPoints[_spawnIterator].position;
            _spawnIterator = ++_spawnIterator % _spawnPoints.Count;
            _prefabIterator = ++_prefabIterator % _prefabs.Count;
            return obj;
        }
    }
}