using System;
using UnityEngine;

namespace Core.StatsSystem
{
    public class Stat : MonoBehaviour
    {
        public event Action<float> ValueChanged;
        public string ID;
        public float Value => _value;

        [SerializeField] private float _value;

        public void Initiliaze()
        {
        }

        public float ModifyValue(float value)
        {
            _value += value;
            
            ValueChanged?.Invoke(Value);
            return Value;
        }
    }
}