using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Core.StatsSystem
{
    public class StatsSystem : IDisposable
    {
        private Dictionary<string, Stat> _stats = new();
        
        public StatsSystem(Transform abilitiesRoot)
        {
            var stats = abilitiesRoot.GetComponentsInChildren<Stat>();

            foreach (var stat in stats)
            {
                stat.Initiliaze();
                _stats.Add(stat.ID, stat);
            }
        }

        public Stat GetStat(string id)
        {
            return _stats.GetValueOrDefault(id);
        }

        public float ModifyStat(string statId, float value)
        {
            float modified = 0;
            if (_stats.TryGetValue(statId, out var stat))
            {
                modified = stat.ModifyValue(value);
            }

            return stat == null? throw new WarningException("Stat doesn't exist") : modified;
        }
        
        public void Dispose()
        {
            _stats.Clear();
        }
    }
}