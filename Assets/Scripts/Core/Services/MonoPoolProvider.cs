using System.Collections.Generic;
using Core.Pool;
using UnityEngine;
using VContainer;

namespace Core.Services
{
    public class MonoPoolProvider
    {
        [Inject] private IObjectResolver _resolver;
        private const int InitialCapacity = 4;

        private readonly Dictionary<string, IAbstractPool> _pools = new(InitialCapacity);

        public IPool<T> ResolveMonoPool<T>(T prefab, Transform parent = null) where T : MonoBehaviour, IReusable
        {
            if (_pools.ContainsKey(prefab.PoolID) == false)
            {
                var monoPool = new MonoObjectsPool<T>(_resolver, InitialCapacity, prefab, parent);
                _pools.Add(prefab.PoolID, monoPool);
            }

            var b = _pools[prefab.PoolID];
            var result = b as IPool<T>;

            return result;
        }
    }
}