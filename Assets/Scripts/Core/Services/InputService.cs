using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Services
{
    public class InputService : MonoBehaviour
    {
        public event Action<KeyCode> KeyPressed;
        [SerializeField] private List<KeyCode> _keyCodesToListen;

        private void Update()
        {
            foreach (var keyCode in _keyCodesToListen)
            {
                if (Input.GetKey(keyCode))
                {
                    KeyPressed?.Invoke(keyCode);        
                }
            }
        }
    }
}