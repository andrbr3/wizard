using System;
using UnityEngine;

namespace Core.Utils
{
    public class ColliderHelper : MonoBehaviour
    {
        public Collider Collider
        {
            get
            {
                if (_collider == null)
                {
                    _collider = GetComponent<Collider>();
                }

                return _collider;
            }
        }
    
        private Collider _collider;
    
        public event Action<Collision> CollisionEnterEvent;
        public event Action<Collision> CollisionExitEvent;
        public event Action<Collision> CollisionStayEvent;

        public event Action<Collider> TriggerEnterEvent;
        public event Action<Collider> TriggerExitEvent;
        public event Action<Collider> TriggerStayEvent;

        private void OnCollisionEnter(Collision other) => CollisionEnterEvent?.Invoke(other);
        private void OnCollisionExit(Collision other) => CollisionExitEvent?.Invoke(other);
        private void OnCollisionStay(Collision other) => CollisionStayEvent?.Invoke(other);
        private void OnTriggerEnter(Collider other) => TriggerEnterEvent?.Invoke(other);
        private void OnTriggerStay(Collider other) => TriggerStayEvent?.Invoke(other);
        private void OnTriggerExit(Collider other) => TriggerExitEvent?.Invoke(other);      
    }
}