namespace Core.Utils
{
    public static class Constants
    {
        public static string HealthStatID = "Health";
        public static string DefenseStatID = "Defense";
        public static string MoveSpeedStatID = "MoveSpeed";
    
        public static string MoveAbilityID = "Move";
        public static string DamageRecieveAbilityID = "DamageRecieve";
        public static string ColliderDamageAbilityID = "ColliderDamage";
        public static string RotateAbilityID = "Rotate";
        public static string LookAtRotateAbilityID = "LookAtRotate";
    }
}