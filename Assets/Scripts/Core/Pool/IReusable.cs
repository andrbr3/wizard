namespace Core.Pool
{
    public interface IReusable
    {
        string PoolID { get; }

        void Activate();
        void Release();
    }
}