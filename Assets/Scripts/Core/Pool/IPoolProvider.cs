using UnityEngine;

namespace Core.Pool
{
    public interface IPoolProvider
    {
        IPool<T> ResolveMonoPool<T>() where T : MonoBehaviour, IReusable;
    }
}