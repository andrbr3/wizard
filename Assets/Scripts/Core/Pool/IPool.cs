using UnityEngine;

namespace Core.Pool
{
    public interface IPool<T> : IAbstractPool where T : MonoBehaviour
    {
        bool IsInitialized { get; }
        T Get();
        void Put(T instance);
    }
}