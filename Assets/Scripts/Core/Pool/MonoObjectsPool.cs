using System;
using System.Collections.Generic;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core.Pool
{
    public class MonoObjectsPool<T> : IPool<T> where T : MonoBehaviour, IReusable
    {
        [Inject] private IObjectResolver _resolver;
        
        public bool IsInitialized => _isInitialized;

        private readonly Stack<T> _stack;
        private T _prefab;
        private Transform _poolParent;
        private bool _isInitialized;

        public MonoObjectsPool(IObjectResolver resolver, int initialCapacity, T prefab, Transform poolParent)
        {
            _resolver = resolver;
            _stack = new Stack<T>(initialCapacity);
            
            _prefab = prefab;
            _poolParent = poolParent;
            _isInitialized = true;
        }

        public T Get()
        {
            if (_prefab == null)
            {
                throw new NullReferenceException("You tried to get an instance from non-initialized pool");
            }

            T result;

            if (_stack.Count > 0)
            {
                result = _stack.Pop();
            }
            else
            {
                result = _resolver.Instantiate(_prefab, _poolParent);
                result.name = _prefab.name;
            }

            result.Activate();
            return result;
        }

        public void Put(T instance)
        {
            if (_stack.Contains(instance))
            {
                return;
            }

            instance.transform.SetParent(_poolParent);
            instance.Release();

            _stack.Push(instance);
        }
    }
}