using UnityEngine;

namespace Core.CharacterBehaviours
{
    public abstract class BaseCharacterBehaviour :  MonoBehaviour
    {
        public abstract void Initialize(Core.Character.BaseCharacter character = null);
        public abstract void Deinitialize();
    }
}