using UnityEngine;

namespace Factory
{
    public interface IFactory<T>
    {
        T Create(T prefab, Transform parent);
    }
}