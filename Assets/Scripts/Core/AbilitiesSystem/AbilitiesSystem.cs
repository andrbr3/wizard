using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.AbilitiesSystem
{
    public class AbilitiesSystem : IDisposable
    {
        public BaseAbility CurrentAbility => _activeAbilities[_currentAbility];
        
        private Dictionary<string, BaseAbility> _abilities = new();

        private List<BaseAbility> _activeAbilities = new();
        private Character.BaseCharacter _character;
        private int _currentAbility;
        private Transform _parent;
        
        public AbilitiesSystem(Transform abilitiesRoot, Character.BaseCharacter character, Transform runtime)
        {
            _character = character;
            _parent = runtime;
            
            var abilities = abilitiesRoot.GetComponentsInChildren<BaseAbility>();
            foreach (var ability in abilities)
            {
                ability.Initialize(character);
                _abilities.Add(ability.ID, ability);

                if (ability.IsUsable)
                {
                    _activeAbilities.Add(ability);
                }
            }
        }

        public void NextAbility()
        {
            _currentAbility = ++_currentAbility % _activeAbilities.Count;
        }
        
        public void PrevAbility()
        {
            --_currentAbility;
            
            if (_currentAbility < 0)
            {
                _currentAbility = _activeAbilities.Count - 1;
            }
        }

        public void UseCurrentAbility()
        {
            CurrentAbility.Use(new BaseAbilityData(_character.ShootPoint, _parent.transform));
        }

        public void UseAbility(string id, IExtraData data = null)
        {
            if (_abilities.TryGetValue(id, out var ability))
            {
                ability.Use(data);
            }
        }

        public void Dispose()
        {
            foreach (var ability in _abilities)
            {
                ability.Value.Deinitialize();
            }
            
            _abilities.Clear();
        }
    }
}