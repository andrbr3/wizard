using UnityEngine;

namespace Core.AbilitiesSystem
{
    public struct BaseAbilityData : IExtraData
    {
        public Transform ShootPoint;
        public Transform Parent;

        public BaseAbilityData(Transform shootPoint, Transform parent)
        {
            Parent = parent;
            ShootPoint = shootPoint;
        }
    }
}