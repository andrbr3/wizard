using System;
using UnityEngine;

namespace Core.AbilitiesSystem
{
    public abstract class BaseAbility: MonoBehaviour
    {
        public string ID;
        public bool IsUsable;
        public float Cooldown;
        
        private DateTime _lastLaunchTime = DateTime.Now;

        protected virtual bool CanUse()
        {
            return _lastLaunchTime + TimeSpan.FromSeconds(Cooldown) < DateTime.Now;
        }

        
        public abstract void Initialize(Character.BaseCharacter character);

        public virtual void Use(IExtraData extraData)
        {
            _lastLaunchTime = DateTime.Now;
        }
        
        public abstract void Deinitialize();
    }
}