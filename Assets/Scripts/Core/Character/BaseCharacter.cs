using System;
using Core.AbilitiesSystem;
using Core.CharacterBehaviours;
using Core.Pool;
using Core.Utils;
using Game.Utils;
using UnityEngine;
using VContainer;

namespace Core.Character
{
    [RequireComponent(typeof(ColliderHelper))]
    public abstract class BaseCharacter : MonoBehaviour, IReusable
    {
        public string PoolID => _poolID;
        [SerializeField] private string _poolID;
        
        public event Action<BaseCharacter> Death;
        public Rigidbody RigidBody => _rb;
        public ColliderHelper ColliderHelper => _colliderHelper;
        public Transform ShootPoint => _shootPoint;
        public StatsSystem.StatsSystem GetStatsSystem() => _statsSystem;
        public Core.AbilitiesSystem.AbilitiesSystem GetAbilitiesSystem() => _abilitiesSystem;
    
        [SerializeField] private Transform _shootPoint;
        [SerializeField] private Transform _abilitiesRoot;
        [SerializeField] private Transform _statsRoot;
        [SerializeField] private BaseCharacterBehaviour _characterBehaviour;

        [Inject] private RuntimeParent _runtimeParent;
        
        private Rigidbody _rb;
        private ColliderHelper _colliderHelper;
        private Core.AbilitiesSystem.AbilitiesSystem _abilitiesSystem;
        private StatsSystem.StatsSystem _statsSystem;

        private void OnHealthChanged(float value)
        {
            if (value < 0f)
            {
                Death?.Invoke(this);
            }
        }

        public void UseAbility(string abilityID, IExtraData abilityData)
        {
            _abilitiesSystem.UseAbility(abilityID, abilityData);
        }

        public void Activate()
        {
            gameObject.SetActive(true);
            
            if (_rb == null)
            {
                _rb = GetComponent<Rigidbody>();
            }
            if (_colliderHelper == null)
            {
                _colliderHelper = GetComponent<ColliderHelper>();
            }
            _colliderHelper.Collider.enabled = true;

            _abilitiesSystem = new Core.AbilitiesSystem.AbilitiesSystem(_abilitiesRoot, this, _runtimeParent.transform);
            _statsSystem = new StatsSystem.StatsSystem(_statsRoot);
            _characterBehaviour.Initialize(this);

            _statsSystem.GetStat(Constants.HealthStatID).ValueChanged += OnHealthChanged;
        }

        public void Release()
        {
            _statsSystem.GetStat(Constants.HealthStatID).ValueChanged -= OnHealthChanged;
            _characterBehaviour.Deinitialize();
            _abilitiesSystem.Dispose();
            _statsSystem.Dispose();
            
            gameObject.SetActive(false);
            _colliderHelper.Collider.enabled = false;
            _rb.velocity = Vector3.zero;
        }
    }
}