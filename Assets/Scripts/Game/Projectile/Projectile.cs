using Core.Pool;
using Core.Services;
using Core.Utils;
using Cysharp.Threading.Tasks;
using Game.Abilities;
using Game.Characters;
using Game.Utils;
using UnityEngine;
using VContainer;

namespace Game.Projectile
{
    public class Projectile : MonoBehaviour, IReusable
    {
        public string PoolID => _poolID;
        [SerializeField] private string _poolID;

        [Inject] private MonoPoolProvider _poolProvider;
        [SerializeField] private float _speed;
        private Rigidbody _rb;
        private Collider _collider;
        
        private float _damage;

        public void Shoot(Vector3 direction, float damage)
        {
            _damage = damage;
            if (_rb == null)
            {
                _rb = GetComponent<Rigidbody>();
            }
            _rb.AddForce(direction * _speed, ForceMode.Impulse);
            DestroyRoutine().Forget();
        }

        private async UniTask DestroyRoutine()
        {
            await UniTask.WaitForSeconds(3f, cancellationToken: this.GetCancellationTokenOnDestroy());

            var pool = _poolProvider.ResolveMonoPool(this);
            pool.Put(this);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent<EnemyCharacter>(out var bot))
            {
                bot.UseAbility(Constants.DamageRecieveAbilityID, new DamageRecieveAbility.ExtraData(_damage));
            }
        }
    
        public void Activate()
        {
            gameObject.SetActive(true);
            if (_collider == null)
            {
                _collider = GetComponent<Collider>();
            }
            _collider.enabled = true;
        }

        public void Release()
        {
            transform.rotation = Quaternion.identity;
            gameObject.SetActive(false);
            _collider.enabled = false;
            _rb.velocity = Vector3.zero;
        }
    }
}