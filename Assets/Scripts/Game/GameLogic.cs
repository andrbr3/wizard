using System;
using System.Collections.Generic;
using System.Threading;
using Cinemachine;
using Core.Services;
using Cysharp.Threading.Tasks;
using Game.Characters;
using Game.Spawners;
using Game.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;
using VContainer.Unity;

namespace Game
{
    public class GameLogic : ITickable, IStartable
    {
        public UserCharacter PlayerCharacter => _user;
    
        private const int ENEMIES_COUNT = 10;
        private const float MIN_ENEMY_SPAWN_INTERVAL = 1.5f;
    
        [Inject] private CameraHolder _cameraHolder;
        [Inject] private UserSpawner _userSpawner;
        [Inject] private EnemySpawner _enemySpawner;
        [Inject] private MonoPoolProvider _poolProvider;

        private List<Core.Character.BaseCharacter> _enemies = new();
        private UserCharacter _user;
        private DateTime _lastSpawnTime;
        private CancellationTokenSource _deathToken = new();
        
        void IStartable.Start()
        {
            _user = _userSpawner.Spawn();
            _user.Death += OnUserDeath;
    
            _cameraHolder.GetComponent<CinemachineVirtualCamera>().LookAt = _user.transform;
            _cameraHolder.GetComponent<CinemachineVirtualCamera>().Follow = _user.transform;
        
            SpawnEnemy();
            SpawnEnemy();
        }
    
        private void SpawnEnemy()
        {
            var enemy = _enemySpawner.Spawn();
            enemy.Death += OnEnemyDeath;
            _enemies.Add(enemy);
        
            _lastSpawnTime = DateTime.Now;
        }

        private void OnEnemyDeath(Core.Character.BaseCharacter character)
        {
            _enemies.Remove(character);
            var enemyChar = (EnemyCharacter)character;
            var pool = _poolProvider.ResolveMonoPool(enemyChar);
            pool.Put(enemyChar);
        }

        private void OnUserDeath(Core.Character.BaseCharacter character)
        {
            OnUserDeath().Forget();
        }

        private async UniTaskVoid OnUserDeath()
        {
            _deathToken?.Cancel();
            _deathToken = new CancellationTokenSource();
            _user.Death -= OnUserDeath;
        
            var pool = _poolProvider.ResolveMonoPool(_user);
            pool.Put(_user);

            await UniTask.WaitForSeconds(2f).AttachExternalCancellation(_deathToken.Token);
            if (_deathToken.IsCancellationRequested)
            {
                return;
            }

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Tick()
        {
            if (_enemies.Count < ENEMIES_COUNT && 
                (DateTime.Now - _lastSpawnTime).Seconds > MIN_ENEMY_SPAWN_INTERVAL)
            {
                SpawnEnemy();            
            }
        }
    }
}