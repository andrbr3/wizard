using System;
using UnityEngine;

namespace Game.Behaviours.UserInput
{
    [Serializable]
    public class KeyCodeActionPair
    {
        public KeyCode KeyCode;
        public ActionsEnumrable ActionType;
    }
}