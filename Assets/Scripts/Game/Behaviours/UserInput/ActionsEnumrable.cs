namespace Game.Behaviours.UserInput
{
    public enum ActionsEnumrable
    {
        Forward = 0,
        Back = 1,
        Left = 2,
        Right = 3,
        UseAbility = 4,
        NextAbility = 5,
        PrevAbility = 6
    }
}