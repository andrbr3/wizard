using System.Collections.Generic;
using System.Linq;
using Core.CharacterBehaviours;
using Core.Services;
using Core.Utils;
using Game.Abilities;
using Game.Utils;
using UnityEngine;
using VContainer;

namespace Game.Behaviours.UserInput
{
    public class UserInputBehaviour : BaseCharacterBehaviour
    {
        [SerializeField] private List<KeyCodeActionPair> _actionsMap = new();

        [Inject] private InputService _inputService;
        private Core.Character.BaseCharacter _character;
        
        public override void Initialize(Core.Character.BaseCharacter character = null)
        {
            _character = character;
            _inputService.KeyPressed += OnKeyPressed;
        }

        private void OnKeyPressed(KeyCode keyCode)
        {
            var action = _actionsMap.FirstOrDefault(x => x.KeyCode == keyCode);
            if (action != default)
            {
                InputHandle(action.ActionType);
            }
        }

        private void InputHandle(ActionsEnumrable action)
        {
            if (action == ActionsEnumrable.Forward || action == ActionsEnumrable.Back)
            {
                var sign = action == ActionsEnumrable.Forward ? 1 : -1;
                var moveSpeed = _character.GetStatsSystem().GetStat(Constants.MoveSpeedStatID);
                var moveData = new MoveAbility.ExtraData(sign * _character.RigidBody.transform.forward,  moveSpeed.Value);
                _character.UseAbility(Constants.MoveAbilityID, moveData);
            }

            if (action == ActionsEnumrable.Left || action == ActionsEnumrable.Right)
            {
                var sign = action == ActionsEnumrable.Left ? -1 : 1;
                var data = new RotateAbility.ExtraData
                {
                    Direction = sign,
                    RigidBody = _character.RigidBody
                };

                _character.UseAbility(Constants.RotateAbilityID, data);
            }

            if (action == ActionsEnumrable.NextAbility)
            {
                _character.GetAbilitiesSystem().NextAbility();
            }
    
            if (action == ActionsEnumrable.PrevAbility)
            {
                _character.GetAbilitiesSystem().PrevAbility();
            }

            if (action == ActionsEnumrable.UseAbility)
            {
                _character.GetAbilitiesSystem().UseCurrentAbility();
            }
        }

        public override void Deinitialize()
        {
            _inputService.KeyPressed -= OnKeyPressed;
        }
    }
}