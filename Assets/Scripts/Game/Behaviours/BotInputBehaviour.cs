using Core.CharacterBehaviours;
using Core.Utils;
using Cysharp.Threading.Tasks;
using Game.Abilities;
using VContainer;

namespace Game.Behaviours
{
    public class BotInputBehaviour : BaseCharacterBehaviour
    {
        [Inject] private GameLogic _gameLogic;
        
        private Core.Character.BaseCharacter _character;
        
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            _character = character;
            
            BotBehaviourRoutine().Forget();
        }

        private async UniTaskVoid BotBehaviourRoutine()
        {
            var player = _gameLogic.PlayerCharacter;
            
            while (true)
            {
                if (_character == null)
                {
                    break;
                }
                
                var moveSpeed = _character.GetStatsSystem().GetStat(Constants.MoveSpeedStatID);
                var direction =  (player.transform.position - _character.RigidBody.position).normalized;
                if (moveSpeed == null)
                {
                    break;
                }
                _character.UseAbility(Constants.MoveAbilityID, new MoveAbility.ExtraData(direction, moveSpeed.Value));
                _character.UseAbility(Constants.LookAtRotateAbilityID, new LookAtRotateAbility.ExtraData(player.transform));

                await UniTask.DelayFrame(1, cancellationToken : this.GetCancellationTokenOnDestroy());
            }
        }

        public override void Deinitialize()
        {
        }
    }
}