using Core.AbilitiesSystem;
using UnityEngine;

namespace Game.Abilities
{
    public class MoveAbility : BaseAbility
    {
        private Core.Character.BaseCharacter _character;
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            _character = character;
        }

        public override void Use(IExtraData data)
        {
            var dataConv = (ExtraData)data;
            _character.RigidBody.AddForce(dataConv.Direction * dataConv.MoveSpeed, ForceMode.VelocityChange);
        }

        public override void Deinitialize()
        {
            
        }

        public struct ExtraData : IExtraData
        {
            public Vector3 Direction;
            public float MoveSpeed;

            public ExtraData(Vector3 direction, float moveSpeed)
            {
                Direction = direction;
                MoveSpeed = moveSpeed;
            }
        }
    }
}