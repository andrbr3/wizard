using Core.AbilitiesSystem;
using Core.Utils;
using Game.Utils;

namespace Game.Abilities
{
    public class DamageRecieveAbility : BaseAbility
    {
        private Core.Character.BaseCharacter _character;
        
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            _character = character;
        }

        public override void Use(IExtraData extraData)
        {
            if (!CanUse())
            {
                return;
            }
            
            var dataConv = (ExtraData)extraData;
            var statsSystem = _character.GetStatsSystem();
            var defenceMultiplier = 1 - statsSystem.GetStat(Constants.DefenseStatID).Value; // 1....0
            statsSystem.ModifyStat(Constants.HealthStatID, -(dataConv.Damage * defenceMultiplier));
           
            base.Use(extraData);
        }

        public override void Deinitialize()
        {
        }
        
        public struct ExtraData : IExtraData
        {
            public float Damage;

            public ExtraData(float damage)
            {
                Damage = damage;
            }
        }
    }
}