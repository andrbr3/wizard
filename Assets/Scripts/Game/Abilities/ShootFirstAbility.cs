using Core.AbilitiesSystem;
using Factory;
using UnityEngine;
using VContainer;

namespace Game.Abilities
{
    public class ShootFirstAbility : BaseAbility
    {
        [Inject] private IFactory<Projectile.Projectile> _projectileFactory;
        [SerializeField] private Projectile.Projectile _projectile;
        [SerializeField] private float _damage;

        private Core.Character.BaseCharacter _character;
        
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            _character = character;
        }

        public override void Use(IExtraData extraData)
        {
            if (!CanUse())
            {
                return;
            }
            
            var dataConv = (BaseAbilityData) extraData;
            var projectile = _projectileFactory.Create(_projectile, dataConv.Parent).GetComponent<Projectile.Projectile>();
            projectile.Shoot(_character.RigidBody.transform.forward, _damage);
            projectile.transform.position = dataConv.ShootPoint.position;
            projectile.transform.rotation = _character.RigidBody.transform.rotation * Quaternion.Euler(90f, 0f, 0f);
            
            base.Use(extraData);
        }

        public override void Deinitialize()
        {
        }
    }
}