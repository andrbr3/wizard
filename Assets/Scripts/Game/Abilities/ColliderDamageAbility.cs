using Core.AbilitiesSystem;
using Core.Utils;
using Game.Characters;
using Game.Utils;
using UnityEngine;

namespace Game.Abilities
{
    public class ColliderDamageAbility : BaseAbility
    {
        [SerializeField] private float _damage;
        private Core.Character.BaseCharacter _character;
        private float _damageAmount;
        
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            _character = character;
            _character.ColliderHelper.CollisionEnterEvent += DoDamage;
            _character.ColliderHelper.CollisionStayEvent += DoDamage;
        }

        private void DoDamage(Collision collideObject)
        {
            if (collideObject.gameObject.TryGetComponent<UserCharacter>(out var playerCharacter))
            {
                playerCharacter.UseAbility(Constants.DamageRecieveAbilityID, new DamageRecieveAbility.ExtraData(_damage));
            }
        }

        public override void Use(IExtraData extraData)
        {
        }

        public override void Deinitialize()
        {
            _character.ColliderHelper.CollisionEnterEvent -= DoDamage;
            _character.ColliderHelper.CollisionStayEvent -= DoDamage;
        }
    }
}