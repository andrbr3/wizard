using Core.AbilitiesSystem;
using UnityEngine;

namespace Game.Abilities
{
    public class LookAtRotateAbility : BaseAbility
    {
        private Core.Character.BaseCharacter _character;
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            _character = character;
        }

        public override void Use(IExtraData extraData)
        {
            var dataConv = (ExtraData) extraData;
            _character.RigidBody.transform.LookAt(dataConv.Target);
        }

        public override void Deinitialize()
        {
        }
        
        public struct ExtraData : IExtraData
        {
            public Transform Target;

            public ExtraData(Transform target)
            {
                Target = target;
            }
        }
    }
}