using Core.AbilitiesSystem;
using UnityEngine;

namespace Game.Abilities
{
    public class RotateAbility : BaseAbility
    {
        [SerializeField] private float _speed;   
        
        public override void Initialize(Core.Character.BaseCharacter character)
        {
            
        }

        public override void Use(IExtraData extraData)
        {
            var dataConv = (ExtraData)extraData;
            var rotationCalc = Quaternion.Euler(0f, dataConv.Direction * Time.deltaTime * _speed, 0f) *
                               dataConv.RigidBody.rotation;
            dataConv.RigidBody.MoveRotation(rotationCalc);
        }

        public override void Deinitialize()
        {
        }
        
        public struct ExtraData : IExtraData
        {
            public int Direction;
            public Rigidbody RigidBody;
            public Transform Target;

            public ExtraData(int direction, Rigidbody rigidBody, Transform target)
            {
                Direction = direction;
                RigidBody = rigidBody;
                Target = target;
            }
        }
    }
}