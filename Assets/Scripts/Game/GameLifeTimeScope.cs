using System.Collections.Generic;
using Core.Services;
using Factory;
using Game.Characters;
using Game.Factories;
using Game.Spawners;
using Game.Utils;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Game
{
    public class GameLifeTimeScope : LifetimeScope
    {
        [SerializeField] private UserSpawner _userSpawner;
        [SerializeField] private EnemySpawner _enemySpawner;
        [SerializeField] private RuntimeParent _runtimeParent;
        [SerializeField] private CameraHolder _cameraHolder;
        [SerializeField] private InputService _inputService;
    
        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<MonoPoolProvider>(Lifetime.Singleton);
            builder.Register<IFactory<UserCharacter>, CharacterFactory>(Lifetime.Singleton);
            builder.Register<IFactory<EnemyCharacter>, EnemyFactory>(Lifetime.Singleton);
            builder.Register<IFactory<Projectile.Projectile>, ProjectileFactory>(Lifetime.Singleton);

            builder.RegisterComponent(_userSpawner);
            builder.RegisterComponent(_enemySpawner);
            builder.RegisterComponent(_runtimeParent);
            builder.RegisterComponent(_cameraHolder);
            builder.RegisterComponent(_inputService);
            
            builder.RegisterEntryPoint<GameLogic>().AsSelf();
        }
    }
}