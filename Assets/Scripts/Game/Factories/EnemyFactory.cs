using Core.Pool;
using Core.Services;
using Factory;
using Game.Characters;
using UnityEngine;
using VContainer;

namespace Game.Factories
{
    public class EnemyFactory : IFactory<EnemyCharacter>
    {
        [Inject] private MonoPoolProvider _poolProvider;
        private IPool<EnemyCharacter> _pool;

        public EnemyCharacter Create(EnemyCharacter prefab, Transform parent)
        {
            _pool = _poolProvider.ResolveMonoPool(prefab, parent);
        
            return _pool.Get();
        }
    }
}