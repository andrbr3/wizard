using Core.Pool;
using Core.Services;
using Factory;
using UnityEngine;
using VContainer;

namespace Game.Factories
{
    public class ProjectileFactory : IFactory<Projectile.Projectile>
    {
        [Inject] private MonoPoolProvider _poolProvider;
        private IPool<Projectile.Projectile> _pool;

        public Projectile.Projectile Create(Projectile.Projectile prefab, Transform parent)
        {
            _pool = _poolProvider.ResolveMonoPool(prefab, parent);
            
            return _pool.Get();
        }
    }
}