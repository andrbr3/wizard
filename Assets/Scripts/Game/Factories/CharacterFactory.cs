using Core.Pool;
using Core.Services;
using Factory;
using Game.Characters;
using UnityEngine;
using VContainer;

namespace Game.Factories
{
    public class CharacterFactory : IFactory<UserCharacter>
    {
        [Inject] private MonoPoolProvider _poolProvider;
        private IPool<UserCharacter> _pool;

        public UserCharacter Create(UserCharacter prefab, Transform parent)
        {
            if (_pool == null)
            {
                _pool = _poolProvider.ResolveMonoPool(prefab, parent);
            }
        
            return _pool.Get();
        }
    }
}